#include "row.h"

#include <sstream>

namespace shdb {

template<class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};

template<class... Ts>
overloaded(Ts...)->overloaded<Ts...>;

std::string to_string(const Row &row)
{
    std::stringstream stream;
    stream << '[';
    for (size_t index = 0; index < row.size(); ++index) {
        const auto &value = row[index];
        std::visit(
            overloaded{
                [&](const Null &value) { stream << "NULL"; },
                [&](const uint64_t &value) { stream << value; },
                [&](const bool &value) { stream << (value ? "true" : "false"); },
                [&](const std::string &value) { stream << value; },
            },
            value);
        if (index < row.size() - 1) {
            stream << ',';
        }
    }
    stream << ']';
    return stream.str();
}

bool Null::operator==(const Null &other) const
{
    return true;
}

bool Null::operator!=(const Null &other) const
{
    return false;
}

bool RowId::operator==(const RowId &other) const
{
    return page_index == other.page_index && row_index == other.row_index;
}

bool RowId::operator!=(const RowId &other) const
{
    return !(*this == other);
}

}    // namespace shdb
