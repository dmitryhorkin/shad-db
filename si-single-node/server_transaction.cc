
#include <algorithm>
#include <cassert>
#include <cstdio>

#include "log.h"
#include "server_transaction.h"
#include "storage.h"

std::string format_server_transaction_state(ServerTransactionState state) {
  switch (state) {
  case ServerTransactionState::NOT_STARTED:
    return "not_started";
  case ServerTransactionState::OPEN:
    return "open";
  case ServerTransactionState::PREPARED:
    return "prepared";
  case ServerTransactionState::ROLLBACK:
    return "rollback";
  case ServerTransactionState::ROLLED_BACK_BY_SERVER:
    return "rolled_back_by_server";
  case ServerTransactionState::COORDINATOR_COMMITTING:
    return "coordinator_committing";
  case ServerTransactionState::COMMITTED:
    return "committed";
  default:
    return std::string("unknown(") + std::to_string(static_cast<int>(state)) +
           std::string(")");
  }
}

ServerTransaction::ServerTransaction(ActorId self, Storage *storage,
                                     IRetrier *rt)
    : self_(self), storage_(storage), rt_(rt) {}

void ServerTransaction::tick(Timestamp ts, const std::vector<Message> &messages,
                             std::vector<Message> *msg_out) {
  for (const auto &msg : messages) {
    switch (state_) {
    case ServerTransactionState::NOT_STARTED: {
      process_message_not_started(ts, msg);
      break;
    }
    case ServerTransactionState::OPEN: {
      process_message_open(ts, msg);
      break;
    }
    case ServerTransactionState::PREPARED: {
      process_message_prepared(ts, msg);
      break;
    }
    case ServerTransactionState::COORDINATOR_COMMITTING: {
      process_message_coordinator_committing(ts, msg);
      break;
    }
    case ServerTransactionState::ROLLBACK: {
      process_message_rollback(ts, msg);
      break;
    }
    case ServerTransactionState::COMMITTED: {
      process_message_commited(ts, msg);
      break;
    }
    case ServerTransactionState::ROLLED_BACK_BY_SERVER: {
      process_message_rolled_back_by_server(ts, msg);
      break;
    }
    }
  }
  rt_->get_ready(ts, msg_out);
}

void ServerTransaction::report_unexpected_msg(const Message &msg) {
  LOG_ERROR << "[tx " << txid_ << "] node " << msg.source
            << " provides unexpected message of type \""
            << format_message_type(msg.type)
            << "\" for server transaction in state \""
            << format_server_transaction_state(state_) << "\"";
}

void ServerTransaction::process_message_not_started(Timestamp ts,
                                                    const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in NOT_STARTED state.
    default: {
      report_unexpected_msg(msg);
      break;
    }
  }
}

void ServerTransaction::process_message_open(Timestamp ts, const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in OPEN state.
    default: {
      report_unexpected_msg(msg);
      break;
    }
  }
}

void ServerTransaction::process_message_rollback(Timestamp ts,
                                                 const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in ROLLBACK state.
    default: {
      report_unexpected_msg(msg);
      break;
    }
  }
}

void ServerTransaction::process_message_coordinator_committing(
    Timestamp ts, const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in COORDINATOR_COMMITTING state.
    default: {
      report_unexpected_msg(msg);
    }
  }
}

void ServerTransaction::process_message_prepared(Timestamp ts,
                                                 const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in PREPARED state.
    default: {
      report_unexpected_msg(msg);
    }
  }
}

void ServerTransaction::process_message_commited(Timestamp ts,
                                                 const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in COMMITED state.
    default: {
      report_unexpected_msg(msg);
      break;
    }
  }
}

void ServerTransaction::process_message_rolled_back_by_server(
    Timestamp ts, const Message &msg) {
  switch (msg.type) {
    // TODO: implement processing messages in ROLLED_BACK_BY_SERVER state.
    default: {
      report_unexpected_msg(msg);
      break;
    }
  }
}
