
set -e
set -x

usernames=(
  "gepardo"
  "bkuchin"
  "erzyadev"
  "kononovk"
  "zeronsix"
  "nsvasilev"
  "faucct"
  "svlads"
  "d4rk"
  "kitaisreal"
  "Fedya001"
  "AlexJokel"
  "yesenarman"
  "gripon"
  "Kifye"
  "dkhorkin"
  "mbkkt"
  "pervakovg"
  "Panesher"
  "TEduard"
  "alexeysm"
  "bazarinm"

  "dnorlov"
)

for username in ${usernames[@]}
do
  cp ${username}/threads/main.cpp shad-db/threads/main.cpp
  make -C shad-db/threads main
  cp shad-db/threads/main bins/${username}
done
