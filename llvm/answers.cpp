#include "jit.h"
#include "flow.h"
#include "answers.h"

using namespace shdb;
using llvm::CmpInst;

dummy_sgn answer_dummy(Jit &jit)
{
    // Your code goes here
}

constant_sgn answer_constant(Jit &jit, int constant)
{
    // Your code goes here
}

trivial_sgn answer_trivial(Jit &jit)
{
    // Your code goes here
}

uintptr_t answer_sum(Jit &jit, int count)
{
    // Your code goes here
}

call_sgn answer_call(Jit &jit)
{
    // Your code goes here
}

abs_sgn answer_abs(Jit &jit)
{
    // Your code goes here
}

for_sgn answer_for(Jit &jit)
{
    // Your code goes here
}

