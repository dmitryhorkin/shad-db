#pragma once

#include <llvm/ADT/StringRef.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>

#include <memory>

namespace shdb {

class Jit
{
    class JIT;
    std::unique_ptr<JIT> jit;

    uintptr_t get_compiled_raw(llvm::StringRef name);

public:
    llvm::LLVMContext& ctx;
    llvm::IRBuilder<> builder;
    std::unique_ptr<llvm::Module> module;
    llvm::Type* void_type;
    llvm::Type* i8_type;
    llvm::Type* i32_type;
    llvm::Type* i64_type;
    llvm::Type* i8ptr_type;

    Jit();
    ~Jit();
    void finish(bool dump = false);
    void register_symbol(uintptr_t address, llvm::StringRef name);
    llvm::Function* create_function(llvm::StringRef name, llvm::Type* result, std::vector<llvm::Type*> arguments);
    llvm::BasicBlock* create_bb(llvm::StringRef name, llvm::Function* function);
    llvm::Value* create_constant(unsigned width, uint64_t value);

    template <typename T>
    T get_compiled(llvm::StringRef name)
    {
        return reinterpret_cast<T>(get_compiled_raw(name));
    }
};

}
