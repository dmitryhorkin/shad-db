#include "generator.h"

#include <cassert>

namespace shdb {

std::optional<Type> infer_type(std::shared_ptr<Ast> ast, const std::shared_ptr<SchemaAccessor> &schema_accessor)
{
    // Your code goes here
}

JitValue generate(
    Jit &jit, std::shared_ptr<Ast> ast, const JitRow &jitrow, const std::shared_ptr<SchemaAccessor> &schema_accessor)
{
    // Your code goes here
}

}    // namespace shdb
