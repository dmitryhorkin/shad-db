#include "shdb/db.h"
#include "shdb/sql.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void populate(shdb::Sql &sql)
{
    sql.execute("DROP TABLE test_table");
    sql.execute("CREATE TABLE test_table (id uint64, age uint64, name string, girl boolean)");
    sql.execute("INSERT test_table VALUES (0, 20, \"Ann\", 1>0)");
    sql.execute("INSERT test_table VALUES (1, 21, \"Bob\", 1<0)");
    sql.execute("INSERT test_table VALUES (2, 19, \"Sara\", 1>0)");
    sql.execute("INSERT test_table VALUES (3, 20, \"Ann\", 1>0)");
    sql.execute("INSERT test_table VALUES (4, 15, \"Bob\", 1<0)");
    sql.execute("INSERT test_table VALUES (5, 15, \"Bob\", 1<0)");
}

void test_group()
{
    auto db = create_database(1);
    auto sql = shdb::Sql(db);
    populate(sql);

    auto rows1 = std::vector<shdb::Row>{{static_cast<uint64_t>(15), static_cast<uint64_t>(2)},
                                        {static_cast<uint64_t>(19), static_cast<uint64_t>(1)},
                                        {static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
                                        {static_cast<uint64_t>(21), static_cast<uint64_t>(1)}};

    auto result1 = sql.execute("SELECT age, sum(1) FROM test_table GROUP BY age ORDER BY age");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(40)},
                                        {std::string("Bob"), static_cast<uint64_t>(51)},
                                        {std::string("Sara"), static_cast<uint64_t>(19)}};

    auto result2 = sql.execute("SELECT name, sum(age) FROM test_table GROUP BY name ORDER BY name");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
                                        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(2)},
                                        {std::string("Bob"), static_cast<uint64_t>(21), static_cast<uint64_t>(1)},
                                        {std::string("Sara"), static_cast<uint64_t>(19), static_cast<uint64_t>(1)}};

    auto result3 = sql.execute("SELECT name, age, sum(1) FROM test_table GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(20)},
                                        {std::string("Bob"), static_cast<uint64_t>(15)},
                                        {std::string("Bob"), static_cast<uint64_t>(21)},
                                        {std::string("Sara"), static_cast<uint64_t>(19)}};

    auto result4 = sql.execute("SELECT * FROM test_table GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows4, result4);

    auto rows5 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(20), static_cast<uint64_t>(20)},
        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(21), static_cast<uint64_t>(17)},
        {std::string("Sara"), static_cast<uint64_t>(19), static_cast<uint64_t>(19), static_cast<uint64_t>(19)}};

    auto result5 = sql.execute("SELECT name, min(age), max(age), avg(age) FROM test_table GROUP BY name ORDER BY name");
    assert_rows_equal(rows5, result5);

    auto rows6 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
                                        {std::string("Bob"), static_cast<uint64_t>(21), static_cast<uint64_t>(1)}};

    auto result6 = sql.execute("SELECT name, age, sum(1) FROM test_table WHERE age >=20 GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows6, result6);

    auto rows7 = std::vector<shdb::Row>{{std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
                                        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(2)}};

    auto result7 =
        sql.execute("SELECT name, age, sum(1) FROM test_table GROUP BY name, age HAVING sum(1) > 1 ORDER BY name, age");
    assert_rows_equal(rows7, result7);

    auto rows8 = std::vector<shdb::Row>{{std::string("Bob"), static_cast<uint64_t>(66)}};

    auto result8 =
        sql.execute("SELECT name, min(age) + sum(age) FROM test_table GROUP BY name HAVING max(age) - avg(age) > 0");
    assert_rows_equal(rows8, result8);

    std::cout << "Test group passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect("./mydb", 1);
    auto sql = shdb::Sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql.execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_group();
}
