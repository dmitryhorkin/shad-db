
#include "scenario.h"

namespace {

void run_scenario_simple_two_nodes() {
  run_scenario_simple(
    /* name */ "simple two nodes",
    /* key_intervals */ {{KEY_LOWER_BOUND, 0}, {0, KEY_UPPER_BOUND}},
    /* drop_probability */ 0.3);
}

void run_scenario_multiple_transactions_two_nodes() {
  run_scenario_multiple_transactions(
    "multiple transactions two nodes",
    /* key_intervals */ {{KEY_LOWER_BOUND, 0}, {0, KEY_UPPER_BOUND}},
    /* drop_probability */ 0.3);
}

}  // namespace

int main(int argc, char* argv[]) {
  run_scenario_simple_two_nodes();
  run_scenario_multiple_transactions_two_nodes();
  run_scenario_randomized(
      /* n_servers */ 4,
      /* drop_probability */ 0.3,
      /* n_clients */ 8,
      /* transactions per client */ 10,
      /* gets per transaction */ 10,
      /* puts per transaction */ 10);
  return 0;
}
