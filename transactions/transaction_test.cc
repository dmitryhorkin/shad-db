#include <algorithm>
#include <cassert>
#include <random>

#include "client_transaction.h"
#include "discovery.h"
#include "retrier.h"
#include "server_transaction.h"
#include "storage.h"

template <typename Gen>
void maybe_drop(Gen gen, double keep_prob, std::vector<Message> *messages) {
  auto end =
      std::partition(messages->begin(), messages->end(),
                     [&gen, keep_prob](auto) { return gen() < keep_prob; });
  messages->erase(end, messages->end());
}

// ct, st each have n elements - pairwise-correponding client and
// server transactions.
template <typename Gen>
void loop(Gen gen, int n, double keep_prob, std::vector<ClientTransaction *> ct,
          std::vector<ServerTransaction *> st) {
  std::vector<std::vector<Message>> req(n), resp(n);

  std::uniform_real_distribution uniform(0.0, 1.0);
  auto generator = [&gen, &uniform]() { return uniform(gen); };

  for (int iter = 0; iter < 1000; iter++) {
    for (int i = 0; i < n; i++) {
      maybe_drop(generator, keep_prob, &resp[i]);
      // Provide unique timestamps.
      ct[i]->tick(2 * n * iter + 2 * i, resp[i], &req[i]);
      resp[i].clear();
      maybe_drop(generator, keep_prob, &req[i]);
      st[i]->tick(2 * n * iter + 2 * i + 1, req[i], &resp[i]);
      req[i].clear();
    }
  }
}

void TestClientServerTransaction(
  double drop_probability, IRetrier* client_retrier,
  IRetrier* server_retrier) {
  for (int run = 0; run < 1000; run++) {
    std::minstd_rand0 gen(1234 + run);

    const ActorId client_id = 10;
    const ActorId node_id = 1;

    Discovery discovery(node_id);

    ClientTransactionSpec spec{1,  // start timestamp
                               10, // commit timestamp
                               {   // Get
                                {
                                    1, // start timestamp
                                    1, // key
                                },
                                {
                                    2, // start_timestamp
                                    2, // key
                                }},
                               {// Put
                                {
                                    1, // start timestamp
                                    1, // key
                                    15 // value
                                },
                                {
                                    5, // start_timestamp
                                    3, // key
                                    12 // value
                                }},
                               ClientTransactionSpec::COMMIT};

    Storage storage;
    ServerTransaction st(node_id, &storage, server_retrier);
    ClientTransaction ct(client_id, spec, &discovery, client_retrier);

    loop(gen, 1, 1.0 - drop_probability, {&ct}, {&st});

    assert(ct.state() == ClientTransactionState::COMMITTED);
    assert(st.state() == ServerTransactionState::COMMITTED);

    auto res = ct.export_results();
    assert(res.read_timestamp >= 1);
    assert(res.commit_timestamp >= 10);

    assert(res.gets.size() == 2);
    assert(res.puts.size() == 2);
    assert(res.gets[0].value == 0);
    assert(res.gets[1].value == 0);
  }
}

void TestClientServerTwoConflictingTransactions(
  double drop_probability, IRetrier* retrier[4]) {
  int outcome[2] = {0, 0};
  const int nruns = 500;

  const ActorId client1 = 11;
  const ActorId client2 = 12;
  const ActorId node_id = 1;

  for (int run = 0; run < nruns; run++) {
    std::minstd_rand0 gen(5678 + run);

    Discovery discovery(node_id);

    ClientTransactionSpec spec1{1, // start timestamp
                                5, // commit timestamp
                                {
                                    // Get
                                    {
                                        3, // start timestamp
                                        3, // key
                                    },
                                },
                                {
                                    // Put
                                    {
                                        1, // start timestamp
                                        3, // key
                                        15 // value
                                    },
                                },
                                ClientTransactionSpec::COMMIT};

    ClientTransactionSpec spec2{1, // start timestamp
                                5, // commit timestamp
                                {
                                    // Get
                                    {
                                        3, // start timestamp
                                        3, // key
                                    },
                                },
                                {
                                    // Put
                                    {
                                        1, // start timestamp
                                        3, // key
                                        25 // value
                                    },
                                },
                                ClientTransactionSpec::COMMIT};

    Storage storage;
    ServerTransaction st1(node_id, &storage, retrier[0]);
    ClientTransaction ct1(client1, spec1, &discovery, retrier[1]);

    ServerTransaction st2(node_id, &storage, retrier[2]);
    ClientTransaction ct2(client2, spec2, &discovery, retrier[3]);

    std::vector<ClientTransaction *> ct{&ct1, &ct2};
    std::vector<ServerTransaction *> st{&st1, &st2};
    loop(gen, 2, 1 - drop_probability, {&ct1, &ct2}, {&st1, &st2});

    if ((int)ct1.state() > (int)ct2.state()) {
      std::swap(ct[0], ct[1]);
      std::swap(st[0], st[1]);
    }

    if (ct[0]->state() == ClientTransactionState::ROLLED_BACK_BY_SERVER) {
      ++outcome[0];

      // One of the two transactions rolled back.
      assert(ct[0]->state() == ClientTransactionState::ROLLED_BACK_BY_SERVER);
      assert(ct[1]->state() == ClientTransactionState::COMMITTED);

      assert(st[0]->state() == ServerTransactionState::ROLLED_BACK_BY_SERVER);
      assert(st[1]->state() == ServerTransactionState::COMMITTED);
    } else {
      ++outcome[1];

      // Both transactions committed.
      assert(ct[0]->state() == ClientTransactionState::COMMITTED);
      assert(ct[1]->state() == ClientTransactionState::COMMITTED);

      assert(st[0]->state() == ServerTransactionState::COMMITTED);
      assert(st[1]->state() == ServerTransactionState::COMMITTED);

      auto r0 = ct[0]->export_results();
      auto r1 = ct[1]->export_results();
      if (r0.read_timestamp > r1.read_timestamp) {
        std::swap(r0, r1);
      }
      assert(r0.commit_timestamp < r1.read_timestamp);
      assert(r0.puts[0].value == r1.gets[0].value);
    }
  }

  if (drop_probability == 0.0) {
    // Without message loss, only one transaction commits.
    assert(outcome[0] == nruns);
    assert(outcome[1] == 0);
  } else {
    // When message loss is present, sometimes both transactions commit serially.
    assert(outcome[0] > 0);
    assert(outcome[1] > 0);
  }
}
