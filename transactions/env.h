#pragma once

#include <functional>
#include <memory>
#include <queue>
#include <random>
#include <string>
#include <unordered_map>
#include <vector>

#include "clock.h"
#include "message.h"
#include "types.h"

// IActor describes the actor interface. On every tick, an actor node
// processes messages designated to it, and may provide some outgoing
// messages.
//
// Outgoing messages are sent via Env::send() method, see Env class below.
struct IActor {
  virtual ~IActor() = default;

  virtual ActorId get_id() const = 0;
  virtual void on_tick(Clock &clock, std::vector<Message> messages) = 0;
};

// Env passes messages between actors, and simulates message loss and delay.
class Env {
public:
  Env(Timestamp mean_delivery_delay, double drop_probability);

  void register_actor(IActor *actor);

  // Send a message to another node.
  // Messages may be delayed, reordered or lost.
  void send(Message msg);

  void run(int ticks = 10000);

  int get_sent_message_count() const;

private:
  struct SentMessage : public Message {
    Timestamp send_timestamp;
    Timestamp delivery_timestamp;
  };

  struct DeliveredLast {
    bool operator()(const SentMessage &lhs, const SentMessage &rhs) const;
  };

  const double drop_probability_;

  std::mt19937 generator_;
  std::exponential_distribution<double> delivery_delay_distribution_;
  std::uniform_real_distribution<double> drop_distribution_;

  Clock clock_;
  Timestamp timestamp_;
  std::vector<IActor *> actors_;
  std::priority_queue<SentMessage, std::vector<SentMessage>, DeliveredLast>
      messages_queue_;

  std::unordered_map<ActorId, std::vector<Message>> actor_messages_;

  int sent_message_count_ = 0;

  Timestamp generate_delivery_delay();
  bool should_drop();
};

// EnvProxy proxies Env::send() call, populating source field for every message.
class EnvProxy {
public:
  EnvProxy(Env *env, ActorId source);

  void send(Message msg);

private:
  Env *const env_;
  const ActorId source_;
};
